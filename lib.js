const path = require('path')

const regex = /^\d{1,6}$/

function retitle(torrent) {
	if (!torrent.name.match(regex)) return false
	const largest = torrent.content
		.sort((a,b)=> b.size-a.size)
		.shift()
	const largestFilename = Array.isArray(largest.name)?largest.name.pop():largest.name
	const filename = path.parse(largestFilename).name
	if (filename.match(regex)) return false
	return largestFilename.startsWith(torrent.name) ? largestFilename : `${torrent.name} - ${largestFilename}`
}

module.exports = {retitle}