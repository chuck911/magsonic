const fs = require('fs')
const {retitle} = require('./lib')
const MongoClient = require('mongodb').MongoClient
const url = 'mongodb://127.0.0.1:27017'

;(async ()=>{
	const mongo = await MongoClient.connect(url, { useUnifiedTopology:true })
	const table = mongo.db('torrentank').collection('torrents')
	const cursor = table.find().sort({_id:-1})

	while(await cursor.hasNext()) {
		const torrent = await cursor.next()
		const title = retitle(torrent)
		if (title) {
			console.log(torrent.link, torrent.name, title)
			await table.updateOne({_id: torrent._id}, {$set: {title}})
		}
	}
})()