const fs = require('fs')
const MongoClient = require('mongodb').MongoClient
const SonicChannelIngest = require("sonic-channel").Ingest;
const sonicOptions = {host:'127.0.0.1', port: 1491, auth:'S0N1C'}
const url = 'mongodb://127.0.0.1:27017'

const reg = /(ap|bf|S\-Cute|10musume|fc2\-?ppv|[A-Za-z]{3,5})[\-\s_]?[0]*([0-9]+)/gi

const sonicIngest = new SonicChannelIngest(sonicOptions).connect({
	connected: ()=>{
		console.log('search ingest connected')
		// sonicChannelIngest.flushc('emojixd').then(console.log)
		ingest().then(()=>{
			console.log('ingest done')
		})
	}
})

function getKeyText(torrent) {
	const keys = torrent.content
		.filter(file=>file.size>80000000)
		.map(file=>Array.isArray(file.name)?file.name.join(''):file.name)
	keys.unshift(torrent.name)
	let keyString = keys.join(' ')
	let vid, vids = []
	while(vid = reg.exec(keyString)) {
		vids.push(vid[1]+'-'+vid[2].padStart(3,'0'))
	}
	vids = [...new Set(vids)]
	keyString+=' '+vids.join(' ')
	return keyString.replace(/\./g, ' ')
}

// , {LANG: "eng"}
// , {lang: "none"}

async function ingest() {
	const mongo = await MongoClient.connect(url, { useUnifiedTopology:true })
	const table = mongo.db('torrentank').collection('torrents')
	const cursor = table.find().sort({_id:1})

	while(await cursor.hasNext()) {
		const torrent = await cursor.next()
		const keyText = getKeyText(torrent)
		await sonicIngest.push('magnet', 'index', torrent._id+':'+torrent.link, keyText, {lang:'none'})
		console.log(torrent.link, keyText)
	}
}
