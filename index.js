const Koa = require('koa')
const Router = require('koa-router')
const koaNunjucks = require('koa-nunjucks-2')
const path = require('path')
const bytes = require('bytes')
const filterXSS = require("xss")
const MongoClient = require('mongodb').MongoClient
const ObjectID = require('mongodb').ObjectID
const sonicChannel = require("sonic-channel")
const Redis = require('ioredis')
const samples = require('lodash.samplesize')
const dayjs = require('dayjs')
const got = require('got')
const {retitle} = require('./lib')
const redis = new Redis()
const app = new Koa()
const router = new Router()
const fs = require('fs')
const sonicOptions = {host:'127.0.0.1', port: 1491, auth:'S0N1C'}

const sonicIngest = new sonicChannel.Ingest(sonicOptions).connect({})
const sonicSearch = new sonicChannel.Search(sonicOptions).connect({})

const reg = /(ap|bf|S\-Cute|10musume|fc2\-?ppv|[A-Za-z]{3,5})[\-\s_]?[0]*([0-9]+)/gi

let collection
async function getTorrents() {
	if (collection) return collection
	const url = 'mongodb://127.0.0.1:27017'
	const mongo = await MongoClient.connect(url, { useUnifiedTopology:true })
	collection = mongo.db('torrentank').collection('torrents')
	return collection
}

function getKeyText(torrent) {
	const keys = torrent.content
		.filter(file=>file.size>80000000)
		.map(file=>Array.isArray(file.name)?file.name.join(''):file.name)
	keys.unshift(torrent.name)
	let keyString = keys.join(' ')
	let vid, vids = []
	while(vid = reg.exec(keyString)) {
		vids.push(vid[1]+'-'+vid[2].padStart(3,'0'))
	}
	vids = [...new Set(vids)]
	vids = vids.join(' ')
	if (vids) keyString+=' '+vids
	return {vids, keyText: keyString.replace(/\./g, ' ')}
}

function refineId(str) {
	str = str.toLowerCase()
    const matches = str.match(/([a-z]+)[\-\s_]?0*(\d+)/i)
    if (!matches) return ''
    let [_, prefix, id] = matches
    
    id = id.padStart(3, '0')
    return prefix+'-'+id
}

app.use(koaNunjucks({
	ext: 'njk',
	path: path.join(__dirname, 'views'),
	nunjucksConfig: {}
}))

router.get('/', async (ctx) => {
	await ctx.render('index')
})

router.get('/ingest', async (ctx) => {
	const Torrents = await getTorrents()
	const torrent = await Torrents.findOne({link: ctx.query.link})
	const {keyText, vids} = getKeyText(torrent)
	if (vids) {
		await sonicIngest.push('magnet', 'ids', torrent._id+':'+torrent.link, vids, {lang:'none'})
	}
	await sonicIngest.push('magnet', 'index', torrent._id+':'+torrent.link, keyText, {lang:'none'})
	const title = retitle(torrent)
	if (title) {
		await Torrents.updateOne({_id: torrent._id}, {$set: {title}})
	}
	const id = refineId(torrent.name)
	if (id) {
		try {
		    const res = await got('https://javtxt.com/queryByOid?oid='+id, {responseType: 'json'})
		    const jav_info = res.body
		    if (jav_info) {
		        await Torrents.updateOne({_id:torrent._id}, {$set: { jav_info }})
		    }
		} catch (e) {}
	}
	ctx.body = keyText
})

async function getHotWords() {
	const datestring = (new Date()).toISOString().slice(0,10)
	let hotWords = await redis.zrevrange('magsearch:'+datestring, 0, 100)
	hotWords = hotWords.filter(w=>w.match(/^[\w-_]+$/))
	hotWords = samples(hotWords, 10)
	return hotWords
}

router.get('/search', async (ctx) => {
	let hotWords = []
	const queryReg = /(ap|bf|S\-Cute|10musume|fc2\-?ppv|[A-Za-z]{3,5})[\-\s_]?[0]*([0-9]+)/i
	let query = ctx.query.q.trim().replace(/[\[\]\(\)\\\*\+\?<>]/g, '')
	if (!query.length) {
		hotWords = await getHotWords()
		await ctx.render('blank', {torrents:[], query:'', hotWords})
		return
	}
	let _query = query
	let matched = query.match(queryReg)
	if (matched) {
		_query = matched[1]+'-'+matched[2].padStart(3, '0')
	}
	let results
	if (matched && query.match(/^[a-z]+[\-\s_]?\d+\w{0,2}$/i)) {
		results = await sonicSearch.query('magnet', 'ids', _query, {limit: 100})
	} else {
		results = await sonicSearch.query('magnet', 'index', _query, {limit: 100})
	}
	
	const ids = results.map(s => new ObjectID(s.split(':')[0]))
	const Torrents = await getTorrents()
	let torrents = await Torrents.find({_id: {$in: ids}}).toArray()
	const sizeLarge = 10000000000
	const torrents_large = torrents.filter(torrent=>torrent.size>=sizeLarge)
	const torrents_normal = torrents.filter(torrent=>torrent.size<sizeLarge)
	torrents = torrents_normal.concat(torrents_large)
	
	for (const torrent of torrents) {
		torrent.sizeLabel = bytes(torrent.size)
		const files = torrent.content.sort((a,b)=>b.size-a.size)
		if (files[0] && files[0].name) {
			torrent.sample = Array.isArray(files[0].name)?files[0].name.join(''):files[0].name
		} else {
			torrent.sample = '...'
		}
		const reg = matched ? new RegExp('('+matched[1]+'\-?'+matched[2].padStart(3, '0')+')', 'i') : new RegExp(`(${query})`, 'i')
		torrent.name = torrent.name.replace(reg, '<b>$1</b>')
		torrent.sample = torrent.sample.replace(reg, '<b>$1</b>')
		torrent.matchScore = (torrent.name.match(reg)?2:0) + (torrent.sample.match(reg)?1:0)
	}
	torrents.sort((a,b)=>b.matchScore - a.matchScore) 
	const datestring = (new Date()).toISOString().slice(0,10)
	if (torrents.length<=5) {
		hotWords = await getHotWords()
	}
	if (torrents.length && !_query.startsWith('Found')) {
		redis.zincrby('magsearch:'+datestring, 1, _query.toLowerCase())
	}
	await ctx.render('search', {torrents, query, hotWords})
})

router.get('/search/hot', async ctx => {
	const today = (new Date()).toISOString().slice(0,10)
	const yesterday = dayjs(today).subtract(1,'day').format('YYYY-MM-DD')
	const size = 200;
	const hotWords1 = await redis.zrevrange('magsearch:'+today, 0, size)
	const hotWords0 = await redis.zrevrange('magsearch:'+yesterday, 0, size)
	const hotWords = [...new Set(hotWords1.concat(hotWords0))]
		.filter(w=>w.match(/^[\w-_]+$/)).filter(w=>!w.includes('023'))
	ctx.body = hotWords
	await ctx.render('hotwords', {hotWords})
})

app.use(router.routes())
	.use(router.allowedMethods())


app.listen(3106)